package com.mohn93.mcapp

import android.databinding.BaseObservable
import android.databinding.Observable
import android.databinding.ObservableBoolean
import android.databinding.ObservableField

class AnswerState(var selected :ObservableBoolean = ObservableBoolean(),
                  var isRight:ObservableBoolean = ObservableBoolean(),
                  var title:ObservableField<String> = ObservableField())