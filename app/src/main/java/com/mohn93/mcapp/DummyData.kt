package com.mohn93.mcapp

import com.mohn93.mcapp.Models.AnswerModel
import com.mohn93.mcapp.Models.QModel

/**
 * Created by mohn93 on 6/1/18.
 */
object DummyData{
    val questions:List<QModel>
    get() {
        val ans = ArrayList<AnswerModel>()
        ans.add( AnswerModel("First"))
        ans.add( AnswerModel("Second"))
        ans.add( AnswerModel("Third"))
        ans.add( AnswerModel("Forth",true))
        val q = QModel("What is the first dahi?",ans)

        val qList = ArrayList<QModel>()
        qList.add(q)
        qList.add(QModel("What is the first dahi1?",ans))
        qList.add(QModel("What is the first dahi2?",ans))
        qList.add(QModel("What is the first dahi3?",ans))

        return qList
    }

}