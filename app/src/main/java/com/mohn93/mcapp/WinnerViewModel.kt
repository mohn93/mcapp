package com.mohn93.mcapp

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import com.google.gson.GsonBuilder
import com.mohn93.mcapp.Endpoints.QuestionsEndpoint
import com.mohn93.mcapp.Endpoints.WinnerEndpoint
import com.mohn93.mcapp.Models.QModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class WinnerViewModel : ViewModel() {
    val loading = ObservableBoolean(false)

    val submitViewShowed = ObservableBoolean(true)
    val errorViewShowed = ObservableBoolean(false)
    val finishViewShowed = ObservableBoolean(false)

    var submittedCallback:() -> Unit = {}

    fun postWinner(name:String,phoneNumber:String)
    {
        loading.set(true)
        submitViewShowed.set(false)
        val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create()
        val retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        val apiService = retrofit.create(WinnerEndpoint::class.java)
        val call = apiService.postWinner(name,phoneNumber)
        call.enqueue(object: Callback<String> {
            override fun onFailure(call: Call<String>?, t: Throwable?) {
                loading.set(false)
                errorViewShowed.set(true)
                finishViewShowed.set(true)
            }

            override fun onResponse(call: Call<String>?, response: Response<String>?) {
                errorViewShowed.set(false)
                loading.set(false)
                submitViewShowed.set(false)
                finishViewShowed.set(true)
                submittedCallback()
            }

        })
    }

}