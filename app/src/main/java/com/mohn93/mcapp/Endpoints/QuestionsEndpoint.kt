package com.mohn93.mcapp.Endpoints

import com.mohn93.mcapp.Models.QModel
import retrofit2.Call
import retrofit2.http.GET

interface QuestionsEndpoint {
    @GET("questions")
    fun getQuestions(): Call<List<QModel>>
}