package com.mohn93.mcapp.Endpoints

import com.mohn93.mcapp.Models.QModel
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface WinnerEndpoint {
    @FormUrlEncoded
    @POST("winner")
    fun postWinner(@Field("name") name:String,@Field("phoneNumber") phoneNumber:String): Call<String>
}