package com.mohn93.mcapp

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import com.mohn93.mcapp.Models.QModel
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import com.google.gson.GsonBuilder
import com.mohn93.mcapp.Endpoints.QuestionsEndpoint
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by mohn93 on 5/31/18.
 */
class QuestionsViewModel : ViewModel() {
    var questions = ObservableArrayList<QModel>()
    var currentQ = ObservableField<QModel>()
    var qProgress = ObservableInt(0)
    var currentQIndex = ObservableInt(0)
    val answeredState = ObservableBoolean(false)
    var isWon = true

    val loading = ObservableBoolean(true)
    val failedRequest = ObservableBoolean(false)


     var firstAnswerState = AnswerState()
     var secondAnswerState = AnswerState()
     var thirdAnswerState = AnswerState()
     var forthAnswerState = AnswerState()


    init {
    }

    fun startGetQuestions(){
        loading.set(true)
        val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create()
        val retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        val apiService = retrofit.create(QuestionsEndpoint::class.java)
        val call = apiService.getQuestions()
        call.enqueue(object: Callback<List<QModel>> {
            override fun onFailure(call: Call<List<QModel>>?, t: Throwable?) {
                loading.set(false)
                failedRequest.set(true)
            }

            override fun onResponse(call: Call<List<QModel>>?, response: Response<List<QModel>>?) {
                failedRequest.set(false)
                loading.set(false)
                questions.clear()
                questions.addAll(response!!.body()!!)
                currentQ.set(questions[0])
                fillAnswers()
            }
        })
    }

    private fun fillAnswers() {
        firstAnswerState.isRight.set(currentQ.get()!!.answers[0].isRight)
        firstAnswerState.title.set(currentQ.get()!!.answers[0].answer)
        firstAnswerState.selected.set(false)

        secondAnswerState.isRight.set(currentQ.get()!!.answers[1].isRight)
        secondAnswerState.title.set(currentQ.get()!!.answers[1].answer)
        secondAnswerState.selected.set(false)

        thirdAnswerState.isRight.set(currentQ.get()!!.answers[2].isRight)
        thirdAnswerState.title.set(currentQ.get()!!.answers[2].answer)
        thirdAnswerState.selected.set(false)

        forthAnswerState.isRight.set(currentQ.get()!!.answers[3].isRight)
        forthAnswerState.title.set(currentQ.get()!!.answers[3].answer)
        forthAnswerState.selected.set(false)

    }

    fun selectAnswer(index: Int) {
        if (!answeredState.get()) {

            answeredState.set(true)
            when (index) {
                0 -> {
                    isWon = if (isWon) firstAnswerState.isRight.get() else false
                    firstAnswerState.selected.set(true)
                }
                1 -> {
                    isWon = if (isWon) secondAnswerState.isRight.get() else false
                    secondAnswerState.selected.set(true)
                }
                2 -> {
                    isWon = if (isWon) thirdAnswerState.isRight.get() else false
                    thirdAnswerState.selected.set(true)
                }
                3 -> {
                    isWon = if (isWon) forthAnswerState.isRight.get() else false
                    forthAnswerState.selected.set(true)
                }
            }
        }
    }

    fun nextQ() {
        answeredState.set(false)
        currentQIndex.set(currentQIndex.get() + 1)
        currentQ.set(questions[currentQIndex.get()])
        qProgress.set(((currentQIndex.get() / 4.0) * 100).toInt())
        fillAnswers()
    }

}
