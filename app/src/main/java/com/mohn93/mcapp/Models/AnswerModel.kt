package com.mohn93.mcapp.Models

import com.google.gson.annotations.SerializedName

/**
 * Created by mohn93 on 5/31/18.
 */
data class AnswerModel(@SerializedName("answer") var answer : String,
                       @SerializedName("isRight")var isRight:Boolean = false)