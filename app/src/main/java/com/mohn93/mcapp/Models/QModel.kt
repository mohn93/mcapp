package com.mohn93.mcapp.Models

import com.google.gson.annotations.SerializedName

/**
 * Created by mohn93 on 6/1/18.
 */
data class QModel(@SerializedName("title") var title: String, @SerializedName("answers") var answers: List<AnswerModel>)
