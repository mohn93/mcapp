package com.mohn93.mcapp.Presentation

import android.animation.Animator
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.mohn93.mcapp.QuestionsViewModel
import com.mohn93.mcapp.R
import com.mohn93.mcapp.databinding.FragmentQuestionsBinding


class QuestionsFragment : Fragment() {

    private var mParam1: String? = null
    private var mParam2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    lateinit var viewModel: QuestionsViewModel
    lateinit var questionsCallback: (Boolean) -> Unit

    @BindView(R.id.first_choice)
    lateinit var firstChoice: View
    @BindView(R.id.second_choice)
    lateinit var secondChoice: View
    @BindView(R.id.third_choice)
    lateinit var thirdChoice: View
    @BindView(R.id.forth_choice)
    lateinit var forthChoice: View


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentQuestionsBinding>(
                inflater, R.layout.fragment_questions, container, false)

        val view = binding.root
        viewModel = ViewModelProviders.of(this).get(QuestionsViewModel::class.java)
        binding.viewModel = viewModel
        ButterKnife.bind(this, view)
        viewModel.startGetQuestions()

        return view
    }

    @OnClick(R.id.reload_btn)
    internal fun getQuestions() {
        viewModel.startGetQuestions()
    }

    @OnClick(R.id.first_choice)
    internal fun firstChoiceClick() {
        viewModel.selectAnswer(0)
    }

    @OnClick(R.id.second_choice)
    internal fun secondChoiceClick() {
        viewModel.selectAnswer(1)
    }

    @OnClick(R.id.third_choice)
    internal fun thirdChoiceClick() {
        viewModel.selectAnswer(2)
    }

    @OnClick(R.id.forth_choice)
    internal fun forthChoiceClick() {
        viewModel.selectAnswer(3)
    }


    @OnClick(R.id.next_q)
    internal fun nextQClick() {
        if (viewModel.currentQIndex.get() != viewModel.questions.count() - 1) {
            animateNextQuestions()
        } else {
            questionsCallback(viewModel.isWon)
        }
    }

    private fun animateNextQuestions() {
        firstChoice.animate().setDuration(300).alpha(0f).translationX(300f)
        secondChoice.animate().setDuration(300).alpha(0f).setStartDelay(100).translationX(300f)
        thirdChoice.animate().setDuration(300).alpha(0f).setStartDelay(200).translationX(300f).start()
        forthChoice.animate().setDuration(300).alpha(0f).setStartDelay(300).translationX(300f).setListener(enterAnimListener).start()
    }

    var enterAnimListener = object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {
        }

        override fun onAnimationEnd(animation: Animator?) {
            viewModel.nextQ()
            firstChoice.translationX = -300f
            firstChoice.alpha = 0f
            firstChoice.animate().setDuration(300).alpha(1f).translationX(0f).start()

            secondChoice.translationX = -300f
            secondChoice.alpha = 0f
            secondChoice.animate().setDuration(300).alpha(1f).setStartDelay(100).translationX(0f).start()

            thirdChoice.translationX = -300f
            thirdChoice.alpha = 0f
            thirdChoice.animate().setDuration(300).alpha(1f).setStartDelay(200).translationX(0f).start()

            forthChoice.translationX = -300f
            forthChoice.alpha = 0f
            forthChoice.animate().setDuration(300).alpha(1f).setStartDelay(300).setListener(null).translationX(0f).start()

        }

        override fun onAnimationCancel(animation: Animator?) {
        }

        override fun onAnimationStart(animation: Animator?) {
        }

    }

    companion object {

        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        fun newInstance(param1: String, param2: String): QuestionsFragment {
            val fragment = QuestionsFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}
