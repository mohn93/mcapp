package com.mohn93.mcapp.Presentation

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.mohn93.mcapp.R

class MainActivity : AppCompatActivity() {

    var getStartedFragment=GettingStartedFragment.newInstance("","")
    var questionsFragment=QuestionsFragment.newInstance("","")
    var endFragment=EndFragment.newInstance("","")
    var wonFragment=WonFragment.newInstance("","")

    @BindView(R.id.frame)
    lateinit var frame:FrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this,this)

        val trans = supportFragmentManager.beginTransaction()
        trans.setCustomAnimations(R.anim.enter, R.anim.exit)
        trans.replace(R.id.frame,getStartedFragment)
        trans.commit()
        setUpGetStartedCallback()
        setUpQuestionsCallback()

        setUpEndFragmentCallback()
        setUpWonFragmentCallback()

    }

    private fun setUpWonFragmentCallback() {
        wonFragment.retryCallback = {
            val trans = supportFragmentManager.beginTransaction()
            trans.setCustomAnimations(R.anim.enter, R.anim.exit)
            trans.replace(R.id.frame,getStartedFragment)
            trans.commit()
        }
    }

    private fun setUpQuestionsCallback() {
        questionsFragment.questionsCallback = {isWon->Unit
            if (isWon){
                val trans = supportFragmentManager.beginTransaction()
                trans.setCustomAnimations(R.anim.enter, R.anim.exit)
                trans.replace(R.id.frame,wonFragment)
                trans.commit()
            }else{
                val trans = supportFragmentManager.beginTransaction()
                trans.setCustomAnimations(R.anim.enter, R.anim.exit)
                trans.replace(R.id.frame,endFragment)
                trans.commit()
            }

        }
    }

    private fun setUpEndFragmentCallback() {
        endFragment.startAginCallback = {
            val trans = supportFragmentManager.beginTransaction()
            trans.setCustomAnimations(R.anim.enter, R.anim.exit)
            trans.replace(R.id.frame,questionsFragment)
            trans.commit()
        }
    }

    fun setUpGetStartedCallback(){
        getStartedFragment.getStartedCallback = {
            val trans = supportFragmentManager.beginTransaction()
            trans.setCustomAnimations(R.anim.enter, R.anim.exit)
            trans.replace(R.id.frame,questionsFragment)
            trans.commit()
        }
    }
}
