package com.mohn93.mcapp.Presentation

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.mohn93.mcapp.R


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class EndFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    lateinit var startAginCallback: () -> Unit



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_end, container, false)
        ButterKnife.bind(this, v)

        return v
    }

    @OnClick(R.id.start_again_btn)
    public fun startAgain() {
        startAginCallback()
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                EndFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
