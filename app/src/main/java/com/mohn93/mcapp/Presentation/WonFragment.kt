package com.mohn93.mcapp.Presentation

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.mohn93.mcapp.R
import com.mohn93.mcapp.WinnerViewModel
import com.mohn93.mcapp.databinding.FragmentWonBinding


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class WonFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    @BindView(R.id.phone_no_et)
    lateinit var phoneEt: EditText

    @BindView(R.id.name_et)
    lateinit var nameEt: EditText

    lateinit var retryCallback: () -> Unit


    lateinit var viewModel: WinnerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentWonBinding>(
                inflater, R.layout.fragment_won, container, false)
        viewModel = ViewModelProviders.of(this).get(WinnerViewModel::class.java)

        val v = binding.root

        binding.viewModel = viewModel

        viewModel.submittedCallback = {
            nameEt.setText("")
            phoneEt.setText("")
        }

        ButterKnife.bind(this, v)

        return v
    }

    @OnClick(R.id.done_btn)
    fun done() {
        viewModel.postWinner(nameEt.text.toString(), phoneEt.text.toString())
    }

    @OnClick(R.id.replay_btn)
    fun replay() {
        retryCallback()
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                WonFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
